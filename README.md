# Variability-Fabric

[Variability](https://gitlab.com/Cynosphere/Variability) ported to 1.14 (Fabric mod loader)

**Variability will be used as a testing ground for some features before being split off into their own mods now.**
I am doing this because of the lightweight nature and if people want to expand on certain concepts the entirety of Variability won't be needed.

This will also be used as modularity as there's no set in stone config system yet.

## Previous features that are their own mods

* [Paxels](https://github.com/Cynosphere/FabricPaxels)